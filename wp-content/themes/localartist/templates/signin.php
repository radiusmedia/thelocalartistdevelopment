<?php
/*
	Template name: Sign in

	Sign in users
*/
	if (isset($_GET['action']) && $_GET['action'] == 'login') {
		// check login
	}
?>

<div id="register">
	<h2>Sign in</h2>
	<form method="post" action="/signin?action=login">
		<div class="row">
			<label for="username">Username</label>
			<input type="text" name="username" id="username" maxlength="32">
		</div>
		<div class="row">
			<label for="password">Password</label>
			<input type="password" name="password" id="password">
		</div>
		<div class="button">
			<button type="submit">Signin</button>
		</div>
	</form>
</div>