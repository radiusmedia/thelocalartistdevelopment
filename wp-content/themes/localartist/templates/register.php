<?php
/*
	Template name: Register

	Collect new users data
*/

	if (isset($_GET['action']) && $_GET['action'] == 'signup') {
		// Create new user
		// Send welcome email, using mandrill class
		global $wpdb;

		$username 	= $_POST['username'];
		$email 		= $_POST['email'];

		$register_error = '';

		$userid = $wpdb->get_row("SELECT `wp_registered_users`.`id`
			FROM `wp_registered_users`
			WHERE `wp_registered_users`.`username` = '" . $username . "'", ARRAY_A);

		$emailid = $wpdb->get_row("SELECT `wp_registered_users`.`id`
			FROM `wp_registered_users`
			WHERE `wp_registered_users`.`email` = '" . $email . "'", ARRAY_A);

		if (!$userid && !$emailid) {
			$password 	= sha1($_POST['password']);
			//$userid 	= wp_create_user($username, $password, $email);

			$user_data = array(
				'firstname'		=> $_POST['firstname'],
				'lastname'		=> $_POST['lastname'],
				'username'		=> $username,
				'email'			=> $email,
				'postcode'		=> $_POST['postcode'],
				'password'		=> $password,
				'createdDate'	=> date('Y-m-d H:i:s')
			);
			$wpdb->insert('wp_registered_users', $user_data);
			$id = $wpdb->insert_id;
			
			// Add them to newsletter database if they opt in
			if (isset($_POST['signup']) && $_POST['signup'] == '1') {
				$newsletter_data = array(
					'userId'		=> $id,
					'newsletter' 	=> '1'	
				);			
				$wpdb->insert('wp_user_newsletter', $newsletter_data);
			}

			// Send welcome email

			$register_error = '';
		}
		else {
			$register_error = 'Username / Email already exists.';
		}

		if ($register_error == '') {
			wp_safe_redirect('/');
		}
		else {
			wp_safe_redirect('/register?action=error&message=' . base64_encode($register_error));
		}
	}

	$error = '';
	if (isset($_GET['action']) && $_GET['action'] == 'error') {
		$error = '<p class="error">' . base64_decode($_GET['message']) . '</p>';
	}
?>

<div id="register">
	<h2>Register</h2>
	<?php echo $error; ?>
	<form method="post" action="/register?action=signup">
		<div class="row">
			<label for="firstname">First name</label>
			<input type="text" name="firstname" id="firstname">
		</div>
		<div class="row">
			<label for="lastname">Last name</label>
			<input type="text" name="lastname" id="lastname">
		</div>
		<div class="row">
			<label for="username">Username - cannot be changed</label>
			<input type="text" name="username" id="username" maxlength="32">
		</div>
		<div class="row">
			<label for="email">Email address</label>
			<input type="text" name="email" id="email">
		</div>
		<div class="row">
			<label for="postcode">Postcode</label>
			<input type="text" name="postcode" id="postcode">
		</div>
		<div class="row">
			<label for="password">Password</label>
			<input type="password" name="password" id="password">
		</div>
		<div class="row signup">
			<label for="signup"><input type="checkbox" name="signup" id="signup" value="1"> Please sign me up to receive 'the local' newsletter via email.</label>
		</div>
		<p>By registering you agree to our terms and conditions.</p>
		<div class="button">
			<button type="submit">Register</button>
		</div>
	</form>
</div>