<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'thelocalartist');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's|55H4N4I@PlIGDKf/}VTQswj^ZL^?b26j)|0$RZBbp,K.KIu|{Xv+}SVbT_Ydmu');
define('SECURE_AUTH_KEY',  'bn[ynbdomp#-{}-S.o?=sAnAH>GI`R<S~x]zFC9953+Rx+(AKj+I]vk$fz)/K:m5');
define('LOGGED_IN_KEY',    't#w}[jk-5B@jJq5lHs|a$~,Nfs+QnUlkn:|N4{J)kPxCO-VCuXYei7DK{]l)35/:');
define('NONCE_KEY',        '7#=g!Y? `3Y *SnKV.gDY8vUOsA{GNaBLC)lC gEcCRt{lbl#}2x*9H%`h-;C+Ug');
define('AUTH_SALT',        '2dj5Y9(|gu33=YiSe8%0t%WPp%lgB5aY-$$A/_TCCT4jgN,~1hP9)@I!Oeu?&wI?');
define('SECURE_AUTH_SALT', ',j[xAB-w2:Z,!zeg9@w2Tx-V|WijxYW_8]w1AGPSkWP-uY:${tL=TZIJka?]k7X7');
define('LOGGED_IN_SALT',   '<,)R=c<MdbCp0aP%|-PNSjcB(r1<f}n4ny7wHCEpu7&9%[Pxws1Ax**4>r*2Hd?N');
define('NONCE_SALT',       '~rom]=vx/v5xUg-Ko@QH^b21m~&-b[5+BO[l-7LI=)Dh&`+UN!pAzd}l?x.g}D2R');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
